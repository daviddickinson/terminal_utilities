#!/usr/bin/env python
#For easy printing to stderr and py3 compatibility
from __future__ import print_function

#Remove paths from environment vars. As we can't alter a parents environment
#we simply print the new variable to screen so calling scripts can capture this 
#and deal with actually setting things themself

########################################
# SETUP
########################################

#Settings
VERBOSE=False#True
SEP_CHAR=":"

import os
import sys

#Use the standard superfast unique function http://stackoverflow.com/questions/480214/how-do-you-remove-duplicates-from-a-list-in-python-whilst-preserving-order
def makeUnique(seq):
    seen = set()
    seen_add = seen.add
    return [ x for x in seq if not (x in seen or seen_add(x))]

def removeFromList(lst,rem):
   return [y for y in lst if y != rem]

########################################
# MAIN BODY
########################################

#First check we have at least two arguments
narg=len(sys.argv)-1
if VERBOSE: print("Provided with "+str(narg)+" arguments")
if narg < 2:
   print("ERROR: remove_from_path requires at least two arguments",file=sys.stderr)
   sys.exit(1)

#Now get the path we're modifying
pathName=sys.argv[1]
if VERBOSE: print("Modifying path "+pathName)

#Now check that this is in the environment
if pathName not in os.environ:
   print("WARNING: path '"+pathName+"' not in environment -- can't remove anything.",file=sys.stderr)
   #This is a success still as we've ensured the passed dir is not in the path variable
   sys.exit(0)

#So now we can get the path -- Note we convert all to absolute paths, probably don't need this.
pathList=makeUnique(map(lambda x: os.path.abspath(x), os.environ[pathName].split(SEP_CHAR)))

#Now we can remove from path
for pth in sys.argv[2:]:
   if VERBOSE:
      if os.path.abspath(pth) not in pathList: print("WARING: path '"+os.path.abspath(pth)+"' not in "+pathName,file=sys.stderr)
   pathList=removeFromList(pathList,os.path.abspath(pth))

#Now we can finally form string
strList=pathName+"="+SEP_CHAR.join(pathList)
print(strList)

sys.exit(0)
